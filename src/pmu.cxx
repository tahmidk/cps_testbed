#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/internet-module.h>
#include <ns3/point-to-point-module.h>
#include <ns3/applications-module.h>

auto main() -> int
{
    ns3::LogComponentEnable("UdpEchoClientApplication", ns3::LOG_LEVEL_INFO);
    ns3::LogComponentEnable("UdpEchoServerApplication", ns3::LOG_LEVEL_INFO);

    auto nodes = ns3::NodeContainer();
    nodes.Create(2);

    auto p2p_helper = ns3::PointToPointHelper();
    p2p_helper.SetDeviceAttribute("DataRate", ns3::StringValue("5Mbps"));
    p2p_helper.SetChannelAttribute("Delay", ns3::StringValue("2ms"));

    auto devices = p2p_helper.Install(nodes);

    auto stack_helper = ns3::InternetStackHelper();
    stack_helper.Install(nodes);

    auto addr_helper = ns3::Ipv4AddressHelper();
    addr_helper.SetBase("10.1.1.0", "255.255.255.0");

    auto interfaces = addr_helper.Assign(devices);

    auto echo_server = ns3::UdpEchoServerHelper(9);

    auto server_apps = echo_server.Install(nodes.Get(1));
    server_apps.Start(ns3::Seconds(1.0));
    server_apps.Stop(ns3::Seconds(10.0));

    auto echo_client = ns3::UdpEchoClientHelper(interfaces.GetAddress(1), 9);
    echo_client.SetAttribute("MaxPackets", ns3::UintegerValue(1));
    echo_client.SetAttribute("Interval", ns3::TimeValue(ns3::Seconds(1.0)));
    echo_client.SetAttribute("PacketSize", ns3::UintegerValue(1024));

    auto client_apps = echo_client.Install(nodes.Get(0));
    client_apps.Start(ns3::Seconds(2.0));
    client_apps.Stop(ns3::Seconds(10.0));

    ns3::Simulator::Run();
    ns3::Simulator::Destroy();
}
